﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon_", menuName = "ScriptableObjects/Equipment/Weapon", order = 1)]
public class WeaponData : ScriptableObject
{
    public string nameOfWeapon = "";
    public int attack = 0;
    public int cost = 0;

}
