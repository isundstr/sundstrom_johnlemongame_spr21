﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public WeaponData thingToPickup;

    WeaponData equippedWeapon;
    public ArmorData equippedArmor;

    int currentAttack;
    int currentDefense;


    // Start is called before the first frame update
    void Start()
    {
        equippedWeapon = thingToPickup;
        print("Attack:" + equippedWeapon.attack + " defense:" + equippedArmor.defense);
        currentAttack = equippedWeapon.attack;
        currentDefense = equippedArmor.defense;
        
    }

    // Update is called once per frame
    void Update()
    {
        currentAttack += 1;
        print(currentAttack);
    }
}
