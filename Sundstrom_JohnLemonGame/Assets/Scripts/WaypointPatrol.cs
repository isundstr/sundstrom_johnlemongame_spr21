﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;

    int m_CurrentWaypointIndex;

    bool flippedDirection = false;

    void Start ()
    {
        navMeshAgent.SetDestination (waypoints[0].position);
    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space)){
            FlipDirection();
        }

        if(navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
        {
            SetNextDestination();
        }
    }

    public void FlipDirection(){
        flippedDirection = !flippedDirection;

        SetNextDestination();
        

    }

    void SetNextDestination(){
        if (flippedDirection){
                m_CurrentWaypointIndex = (m_CurrentWaypointIndex - 1);
                if (m_CurrentWaypointIndex < 0){
                    m_CurrentWaypointIndex = waypoints.Length - 1;
                }
            }
        else
        m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;

        navMeshAgent.SetDestination (waypoints[m_CurrentWaypointIndex].position);
    }
}
