﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update
    Equipment sword;
    Equipment laserCannon;

    int swordAttack = 5;
    int swordSpeed = 2;

    bool swordUsable = true;

    int laserAttack = 9999;
    int laserSpeed = 0;

    bool laserUsable = true;

    public float speed;
    void Start()
    {
        sword = new Equipment(5, 2);

        laserCannon = new Equipment(9999, 0);
    }

    // Update is called once per frame
    void Update()
    {
        sword.StartAttacking();
        laserCannon.StartAttacking();
    }

    public void Jump(){

    }
}
