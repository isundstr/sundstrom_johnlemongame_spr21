﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment
{
    public int attackPower;
    public int attackSpeed;

    public bool usable = true;

    public Equipment(int attack, int speed){
        attackPower = attack;
        attackSpeed = speed;

    }


    public void StartAttacking(){

        Debug.Log("attacK:" + attackPower + " attackSpeed:" + attackSpeed);

    }

}
