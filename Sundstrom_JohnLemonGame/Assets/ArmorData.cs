﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armor_", menuName = "Gobblygook/Equipment/Armor", order = 1)]
public class ArmorData : ScriptableObject
{
    public string nameOfArmor = "";
    public int defense = 0;
    public int cost = 0;

}
